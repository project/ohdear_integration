<?php

// @phpcs:ignoreFile cause of Drupal.Functions.MultiLineFunctionDeclaration.MissingTrailingComma (of commands options).

namespace Drupal\ohdear_integration\Drush\Commands;

use Consolidation\OutputFormatters\StructuredData\RowsOfFields;
use Drupal\Core\State\StateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\ohdear_integration\OhDearInfo;
use Drupal\ohdear_integration\OhDearSdkService;
use Drush\Attributes as CLI;
use Drush\Commands\DrushCommands;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A Drush command file for ohdear_integration.
 */
final class OhdearIntegrationCommands extends DrushCommands {

  use StringTranslationTrait;

  /**
   * Constructs an OhdearIntegrationCommands object.
   */
  public function __construct(
    private readonly StateInterface $state,
    private readonly OhDearSdkService $ohdearSdk,
    private readonly OhDearInfo $ohdearInfo,
  ) {
    parent::__construct();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('state'),
      $container->get('ohdear_sdk'),
      $container->get('ohdear_integration.info'),
    );
  }

  /**
   * Prints the current maintenance status or starts/stops maintenance.
   *
   * @param array $options
   *   An associative array of options.
   *
   * @return \Consolidation\OutputFormatters\StructuredData\RowsOfFields
   *   A list of OhDear maintenance windows or activity description.
   */
  #[CLI\Command(name: 'ohdear:maintenance', aliases: [
    'ohdear-maintenance',
    'ohdear-m',
    'ohdear:m',
  ])]
  #[CLI\Option(name: 'start', description: 'Starts the maintenance window in OhDear app.')]
  #[CLI\Option(name: 'stop', description: 'Stops the maintenance window in OhDear app.')]
  #[CLI\Option(name: 'with-drupal', description: 'Puts drupal in or out of maintenance mode.')]
  #[CLI\Option(name: 'time-length', description: 'Time length for the maintenance period in seconds (default: 1 hour).')]
  #[CLI\Option(name: 'number', description: 'Number of maintenance windows to show (default: 5).')]
  #[CLI\Usage(name: 'ohdear:maintenance', description: 'Prints the current maintenance status or starts/stops maintenance and last 5 (default number) maintenance periods.')]
  #[CLI\Usage(name: 'ohdear:maintenance --start --with-drupal', description: 'Starts the maintenance window in OhDear app and puts drupal in (or out of) maintenance mode (for --stop flag) .')]
  #[CLI\FieldLabels(labels: [
    'id' => 'Id',
    'site_id' => 'Site Id',
    'starts_at' => 'Starts at',
    'ends_at' => 'Ends at',
  ])]
  #[CLI\DefaultTableFields(fields: ['id', 'site_id', 'starts_at', 'ends_at'])]
  #[CLI\FilterDefaultField(field: 'id')]
  public function maintenance(array $options = [
    'start' => NULL,
    'stop' => NULL,
    'with-drupal' => NULL,
    'time-length' => 3600,
    'number' => 5,
  ]) {
    $ohDear = $this->ohdearSdk->getOhDear();
    $siteId = $this->ohdearSdk->getSiteId();
    $site = $ohDear->site($siteId);
    // @see https://ohdear.app/docs/integrations/the-oh-dear-php-sdk#maintenance-windows
    if (!$options['start'] && !$options['stop']) {
      // Just print out current maintenance window status.
      $maintenanceWindows = $this->getMaintenancePeriods($siteId, $options['number']);
      return new RowsOfFields($maintenanceWindows);
    }
    elseif ($options['start'] && $options['stop']) {
      $this->logger->warning('Only one of --start or --stop flags can be used.');
      $maintenanceWindows = $this->getMaintenancePeriods($siteId, $options['number']);
      return new RowsOfFields($maintenanceWindows);
    }
    elseif ($options['start']) {
      $time_length = $options['time-length'] ?? 3600;
      $maintenance_window = $ohDear->startSiteMaintenance($siteId, $time_length);
      if ($options['with-drupal']) {
        $this->state->set('system.maintenance_mode', 1);
      }
      $this->output()->writeln($this->t('Maintenance period @id started for @site. It ends at @date.', [
        '@id' => $maintenance_window->id,
        '@site' => $site->url,
        '@date' => $maintenance_window->endsAt ?? '',
      ]));
      return NULL;
    }
    elseif ($options['stop']) {
      $ohDear->stopSiteMaintenance($siteId);
      if ($options['with-drupal']) {
        $this->state->set('system.maintenance_mode', 0);
      }
      $this->output()->writeln($this->t('Maintenance period stopped for @site.', [
        '@site' => $site->url,
      ]));
      return NULL;
    }

    $this->logger->error('Something went wrong. No condition was met.');
    return NULL;
  }

  /**
   * Gets maintenance period for specific site.
   *
   * @param int|null $siteId
   *   Site id.
   * @param int $num
   *   Number of maintenance windows.
   *
   * @return array
   *   Array of last maintenance periods.
   *
   * @throws \Exception
   */
  protected function getMaintenancePeriods(?int $siteId = NULL, int $num = 5) {
    $siteId = $this->getSiteId($siteId);
    $ohDear = $this->ohdearSdk->getOhDear();
    $maintenanceWindows = array_slice(array_reverse($ohDear->maintenancePeriods($siteId)), 0, $num, TRUE);
    if (empty($maintenanceWindows)) {
      $this->logger->warning('No maintenance window found.');
    }
    $rows = [];
    foreach ($maintenanceWindows as $window) {
      // @todo Add a check if a maintenance is currently active.
      $rows[] = [
        'id' => $window->id,
        'site_id' => $window->siteId,
        'starts_at' => $window->startsAt,
        'ends_at' => $window->endsAt ?? '',
      ];
    }
    return $rows;
  }

  /**
   * Helper function to get site id.
   *
   * @param int|null $siteId
   *   Site id or null if configured site is wanted.
   *
   * @return int
   *   Will return site id integer.
   *
   * @throws \Exception
   */
  protected function getSiteId(?int $siteId = NULL) {
    return $siteId ?? $this->ohdearSdk->getSiteId();
  }

  /**
   * Prints current site id and url.
   *
   * @return \Consolidation\OutputFormatters\StructuredData\RowsOfFields
   *   A list of OhDear checks.
   */
  #[CLI\Command(name: 'ohdear:info', aliases: [
    'ohdear-info',
    'ohdear-i',
    'ohdear:i',
  ])]
  #[CLI\Argument(name: 'siteId', description: 'Optional siteId parameter. By default it takes the value provided by configuration but this allows to check other websites as well.')]
  #[CLI\Option(name: 'checks', description: 'Get a list of health checks for site.')]
  #[CLI\Option(name: 'list-sites', description: 'Print all available site ids.')]
  #[CLI\Usage(name: 'ohdear_integration:info', description: 'Prints current site id and url.')]
  #[CLI\FieldLabels(labels: [
    'id' => 'Id',
    'type' => 'Type',
    'label' => 'Label',
    'enabled' => 'Enabled',
    'latest_run_ended_at' => 'Latest run ended at',
    'latest_run_result' => 'Latest run result',
    'summary' => 'Summary',
  ])]
  #[CLI\DefaultTableFields(fields: [
    'id',
    'label',
    'latest_run_ended_at',
    'latest_run_result',
    'summary',
    'enabled',
  ])]
  #[CLI\FilterDefaultField(field: 'id')]
  public function siteInfo(int $siteId = NULL, array $options = [
    'checks' => NULL,
    'list-sites' => NULL,
  ]) {
    $ohDear = $this->ohdearSdk->getOhDear();
    $siteId = $this->getSiteId();
    if (!$siteId) {
      $this->logger()->error('Site id not provided!');
      return NULL;
    }
    $site = $ohDear->site($siteId);
    $this->output()->writeln($this->t('Site ID: @id, Url: @url', [
      '@id' => (string) $site->id,
      '@url' => $site->url,
    ]));
    $this->output()->writeln($this->t('Summarized check results: @results (@date).', [
      '@results' => $site->attributes['summarized_check_result'] ?? 'unknown',
      '@date' => $site->attributes['latest_run_date'] ?? '',
    ]));
    if (isset($options['checks'])) {
      $checks = [];
      foreach ($site->attributes['checks'] as $check) {
        $check['enabled'] = $check['enabled'] ? 'On' : 'Off';
        $checks[] = $check;
      }
      return new RowsOfFields($checks);
    }
    if (isset($options['list-sites'])) {
      $all = array_map(fn ($site) => [
        'id' => (string) $site->id,
        'label' => $site->label ?? $site->url,
      ], $this->ohdearInfo->listSites());
      return new RowsOfFields($all);
    }
    return NULL;
  }

  /**
   * Get broken links for current site.
   *
   * @param int $siteId
   *   The id of the site or empty for current website.
   *
   * @return \Consolidation\OutputFormatters\StructuredData\RowsOfFields
   *   A list of broken links.
   */
  #[CLI\Command(name: 'ohdear:broken-links', aliases: [
    'ohdear-broken-links',
    'ohdear-bl',
    'ohdear:bl',
  ])]
  #[CLI\Argument(name: 'siteId', description: 'Optional siteId parameter. By default it takes the value provided by configuration but this allows to check other websites as well.')]
  #[CLI\Usage(name: 'ohdear:broken-links', description: 'Get broken links for current site.')]
  #[CLI\FieldLabels(labels: [
    'crawledUrl' => 'Crawled URL',
    'statusCode' => 'Status Code',
    'foundOnUrl' => 'Found On URL',
  ])]
  #[CLI\DefaultTableFields(fields: ['crawledUrl', ',statusCode', 'foundOnUrl'])]
  #[CLI\FilterDefaultField(field: 'crawledUrl')]
  public function getBrokenLinks(int $siteId = NULL) {
    $parsedBrokenLinks = array_map(fn ($link) => [
      'crawledUrl' => $link->crawledUrl ?? '',
      'statusCode' => $link->statusCode ?? '',
      'foundOnUrl' => $link->foundOnUrl ?? '',
    ], $this->ohdearInfo->getBrokenLinks($siteId));
    return new RowsOfFields($parsedBrokenLinks);
  }

  /**
   * Get uptime for site. It shows last 7 days by default.
   *
   * @param int $siteId
   *   Site id.
   * @param array $options
   *   List of options.
   *
   * @return \Consolidation\OutputFormatters\StructuredData\RowsOfFields
   *   A list of broken links.
   */
  #[CLI\Command(name: 'ohdear:uptime', aliases: [
    'ohdear-uptime',
    'ohdear-u',
    'ohdear:u',
  ])]
  #[CLI\Argument(name: 'siteId', description: 'Optional siteId parameter. By default it takes the value provided by configuration but this allows to check other websites as well.')]
  #[CLI\Option(name: 'from', description: 'Date from in YmdHis format.')]
  #[CLI\Option(name: 'to', description: 'End date in YmdHis format.')]
  #[CLI\Option(name: 'split', description: 'Time unit to split the interval (day, week, month).')]
  #[CLI\Usage(name: 'ohdear:uptime', description: 'Get uptime for last 7 days (default).')]
  #[CLI\Usage(name: 'ohdear:uptime  --split month --from 20220101000000', description: 'Shows monthly uptime for current site from 1. 1. 2022.')]
  #[CLI\FieldLabels(labels: [
    'datetime' => 'DateTime',
    'percentage' => 'Uptime Percentage',
  ])]
  #[CLI\DefaultTableFields(fields: ['datetime', ',percentage'])]
  #[CLI\FilterDefaultField(field: 'datetime')]
  public function getUptime(int $siteId = NULL, array $options = [
    'from' => NULL,
    'to' => NULL,
    'split' => 'day',
  ]) {
    // @todo Would need from, to and split parameter validation.
    $uptimes = $this->ohdearInfo->getUptime($siteId, $options['from'] ?: date('YmdHis', strtotime('-7 days')), $options['to'], $options['split']);
    $parsed = array_map(fn ($uptime) => [
      'datetime' => $options['split'] == 'hour' ? $uptime->datetime : date('Y-m-d', strtotime($uptime->datetime)),
      'percentage' => $uptime->uptimePercentage,
    ], $uptimes);
    return new RowsOfFields($parsed);
  }

}
