<?php

namespace Drupal\ohdear_integration\Controller;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Cache\CacheableJsonResponse;
use Drupal\Core\Cache\CacheableResponseInterface;
use Drupal\Core\Cache\RefinableCacheableDependencyInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\Core\Render\RenderContext;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\ohdear_integration\OhDearHealthcheckGenerator;
use Drupal\ohdear_integration\OhDearSdkService;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Returns responses for OhDear Integration routes.
 */
class OhDearIntegrationController extends ControllerBase {

  use LoggerChannelTrait;

  /**
   * Healthcheck generator service.
   *
   * @var \Drupal\ohdear_integration\OhDearHealthcheckGenerator
   */
  protected $ohDearGenerator;

  /**
   * The current request.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * The core renderer service.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * The OhDear service development kit and helper service.
   *
   * @var \Drupal\ohdear_integration\OhDearSdkService
   */
  protected $ohDearSdk;

  /**
   * Cache max age ohdear healthcheck json endpoint configuration.
   *
   * @var int|null
   */
  protected $cacheMaxAge;

  /**
   * Timestamp when monitoring results were computed.
   *
   * @var int|string|null
   */
  protected $finishedAt;

  /**
   * Constructs OhDear integration controller.
   *
   * @param \Drupal\ohdear_integration\OhDearHealthcheckGenerator $ohdear_generator
   *   The ohdear helper objects factory service.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger channel factory service.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack service.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The core renderer service.
   * @param \Drupal\ohdear_integration\OhDearSdkService $ohdear_sdk
   *   The ohdear SDK service.
   */
  public function __construct(OhDearHealthcheckGenerator $ohdear_generator, LoggerChannelFactoryInterface $logger_factory, RequestStack $request_stack, RendererInterface $renderer, OhDearSdkService $ohdear_sdk) {
    $this->ohDearGenerator = $ohdear_generator;
    $this->setLoggerFactory($logger_factory);
    $this->requestStack = $request_stack;
    $this->renderer = $renderer;
    $this->ohDearSdk = $ohdear_sdk;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): OhDearIntegrationController {
    return new static(
      $container->get('ohdear_healthcheck.generator'),
      $container->get('logger.factory'),
      $container->get('request_stack'),
      $container->get('renderer'),
      $container->get('ohdear_sdk')
    );
  }

  /**
   * Builds the response.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   Response.
   */
  public function buildJson() {
    // Render response in context to avoid metadata leaks. Some monitoring
    // plugins are rendered on sensor run and cached at that time as well. That
    // metadata is hard to access, so we need rendering in context detour.
    $context = new RenderContext();
    $response = $this->renderer->executeInRenderContext($context, function () {
      if ($this->access($this->currentUser())->isForbidden()) {
        $json_response = new CacheableJsonResponse(
          '{"error": "Access denied!"}',
          403,
          [
            'Cache-Control' => 'private, no-cache, no-store, must-revalidate',
            'Last-Modified' => $this->formatDateString($this->requestStack->getCurrentRequest()->server->get('REQUEST_TIME') ?? time()),
          ],
          TRUE
        );
      }
      else {
        $data = $this->ohDearGenerator->getData();
        $this->finishedAt = json_decode($data, TRUE)['finishedAt'] ?? '0';
        $json_response = new CacheableJsonResponse(
          $data,
          200,
          $this->computeHeaders(),
          TRUE
        );
      }
      // Cache is mostly set outside of context.
      $this->addCacheToResponse($json_response);
      return $json_response;
    });
    // If there is metadata left on the context, apply it on the response.
    if (!$context->isEmpty()) {
      if ($metadata = $context->pop()) {
        $this->refineCacheMetadata($metadata);
        $response->addCacheableDependency($metadata);
      }
    }
    return $response;
  }

  /**
   * Return the header array.
   *
   * @return array
   *   Array of header names as keys and values as values.
   *
   * @throws \Exception
   */
  protected function computeHeaders(): array {
    $max_age = $this->getCacheMaxAge() ?? 0;
    $headers = [];
    if ($max_age) {
      $finishedAt = $this->finishedAt ?? 0;
      if (!$finishedAt) {
        $this->getLogger('ohdear_integration')->warning('FinishedAt time is missing.');
      }
      $expires = (int) $finishedAt + (int) $max_age;
      $headers['Cache-Control'] = "public,max-age=$max_age,s-maxage=$max_age";
      $headers['Last-Modified'] = $this->formatDateString($finishedAt);
      $headers['Expires'] = $this->formatDateString($expires);
      $headers['ETag'] = $finishedAt;
    }
    else {
      $headers['Cache-Control'] = 'private,no-cache,must-revalidate';
      $headers['Last-Modified'] = $this->formatDateString($this->requestStack->getCurrentRequest()->server->get('REQUEST_TIME') ?? time());
    }
    return $headers;
  }

  /**
   * Add cache configuration to response.
   *
   * @param \Drupal\Core\Cache\CacheableResponseInterface $response
   *   Response to add cache to.
   *
   * @return \Drupal\Core\Cache\CacheableResponseInterface
   *   Response with cache added.
   */
  protected function addCacheToResponse(CacheableResponseInterface $response) {
    if (!$response->headers->has('Cache-Control')) {
      $headers = $this->computeHeaders();
      foreach ($headers as $header => $value) {
        $response->headers->set($header, $value);
      }
    }
    $cacheable_metadata = $response->getCacheableMetadata();
    $this->refineCacheMetadata($cacheable_metadata);
    $response->addCacheableDependency($cacheable_metadata);
    return $response;
  }

  /**
   * Adds cache metadata related to the health check.
   *
   * @param \Drupal\Core\Cache\RefinableCacheableDependencyInterface $cacheable_dependency
   *   An object implementing RefinableCacheableDependencyInterface.
   */
  protected function refineCacheMetadata(RefinableCacheableDependencyInterface $cacheable_dependency) {
    $cacheable_dependency->addCacheContexts([
      'url.query_args:oh-dear-health-check-secret',
      'headers:oh-dear-health-check-secret',
      'user.roles',
    ]);
    $cacheable_dependency->addCacheTags(['monitoring_sensor_result']);
    $cacheable_dependency->setCacheMaxAge($this->getCacheMaxAge() ?? 0);
  }

  /**
   * Get cache max age configuration.
   *
   * @return mixed
   *   Return cache max-age.
   */
  protected function getCacheMaxAge() {
    if (!isset($this->cacheMaxAge)) {
      $this->cacheMaxAge = $this->ohDearSdk->getHealthcheckCacheMaxAge();
    }
    return $this->cacheMaxAge;
  }

  /**
   * Access result callback.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   Determines the access to controller.
   */
  public function access(AccountInterface $account) {
    $current_request = $this->requestStack->getCurrentRequest();
    $ohdear_header_secret = $current_request->headers->get('oh-dear-health-check-secret') ?? $current_request->query->get('oh-dear-health-check-secret');
    $ohdear_healthcheck_secret = $this->ohDearSdk->getHealthcheckSecret();
    if (($ohdear_header_secret && $ohdear_header_secret === $ohdear_healthcheck_secret)
      || $account->hasPermission('monitoring reports')) {
      $access_result = AccessResult::allowed();
    }
    else {
      $access_result = AccessResult::forbidden();
      $this->getLogger('ohdear_integration')->notice('Unauthorized access to monitoring results denied.');
    }
    $this->refineCacheMetadata($access_result);
    return $access_result;
  }

  /**
   * Convert timestamp to date string.
   *
   * @param string $timestamp
   *   Timestamp.
   *
   * @return string
   *   Formatted date time.
   *
   * @throws \Exception
   */
  protected function formatDateString(string $timestamp): string {
    $date = new \DateTimeImmutable('@' . $timestamp);
    $date = $date->setTimezone(new \DateTimeZone('UTC'));
    return $date->format('D, d M Y H:i:s') . ' GMT';
  }

}
