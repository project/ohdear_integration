<?php

namespace Drupal\ohdear_integration\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\ohdear_integration\OhDearInfo;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Returns responses for OhDear Integration routes.
 */
class OhdearInfoController extends ControllerBase {

  /**
   * The ohdear_integration.info service.
   *
   * @var \Drupal\ohdear_integration\OhDearInfo
   */
  protected $ohDearInfo;

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * The controller constructor.
   *
   * @param \Drupal\ohdear_integration\OhDearInfo $info
   *   The ohdear_integration.info service.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   */
  public function __construct(OhDearInfo $info, RequestStack $request_stack) {
    $this->ohDearInfo = $info;
    $this->requestStack = $request_stack;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('ohdear_integration.info'),
      $container->get('request_stack')
    );
  }

  /**
   * Get results of OhDear checks for site.
   *
   * @param int|null $site_id
   *   The site id.
   */
  public function info(?int $site_id = NULL) {
    $checks = $this->ohDearInfo->getChecks($site_id);
    array_walk($checks, function (&$check) {
      unset($check['id']);
      unset($check['type']);
      return $check;
    });
    $header = [
      'label' => $this->t('Label'),
      'enabled' => $this->t('Enabled'),
      'latest_run_ended_at' => $this->t('Latest run ended at'),
      'latest_run_result' => $this->t('Result'),
      'summary' => $this->t('Summary'),
    ];
    $build['content'] = [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $checks,
    ];
    return $build;
  }

  /**
   * Title callback for info site.
   *
   * @param int|null $site_id
   *   The site id.
   *
   * @return string
   *   The title.
   */
  public function infoTitle(?int $site_id = NULL) {
    if (!$site_id) {
      $site_id = NULL;
    }
    $label = $this->ohDearInfo->getSiteLabel($site_id);
    return "Info for {$label}";
  }

  /**
   * List of broken links for site.
   *
   * @param int|null $site_id
   *   The site id.
   *
   * @return array
   *   Table of broken links.
   */
  public function brokenLinks(?int $site_id = NULL) {
    if (!$site_id) {
      $site_id = NULL;
    }
    $parsedBrokenLinks = array_map(fn ($link) => [
      'crawledUrl' => $link->crawledUrl ?? '',
      'statusCode' => $link->statusCode ?? '',
      'foundOnUrl' => $link->foundOnUrl ?? '',
    ], $this->ohDearInfo->getBrokenLinks($site_id));
    $header = [
      'crawledUrl' => $this->t('Crawled URL'),
      'statusCode' => $this->t('Status Code'),
      'foundOnUrl' => $this->t('Found on URL'),
    ];
    return [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $parsedBrokenLinks,
    ];
  }

  /**
   * Title callback for broken links site.
   *
   * @param int|null $site_id
   *   The site id.
   *
   * @return string
   *   The title.
   */
  public function brokenLinksTitle(?int $site_id = NULL) {
    if (!$site_id) {
      $site_id = NULL;
    }
    $label = $this->ohDearInfo->getSiteLabel($site_id);
    return "Broken links for {$label}";
  }

  /**
   * Title callback for uptime site.
   *
   * @param int|null $site_id
   *   The site id.
   *
   * @return string
   *   The title.
   */
  public function uptimeTitle(?int $site_id = NULL) {
    if (!$site_id) {
      $site_id = NULL;
    }
    $label = $this->ohDearInfo->getSiteLabel($site_id);
    return "Uptime for {$label}";
  }

  /**
   * List uptimes for the site.
   *
   * @param int|null $site_id
   *   The site id.
   *
   * @return array
   *   Table of uptime percentages per time interval.
   */
  public function uptime(?int $site_id = NULL) {
    $query = $this->requestStack->getCurrentRequest()->query;
    $from = $query->get('from', date('YmdHis', strtotime('-7 days')));
    $to = $query->get('to', date('YmdHis'));
    $split = $query->get('split', 'day');
    $uptimes = $this->ohDearInfo->getUptime($site_id, $from, $to, $split);
    $parsed = array_map(fn ($uptime) => [
      'datetime' => $split == 'hour' ? $uptime->datetime : date('Y-m-d', strtotime($uptime->datetime)),
      'percentage' => $uptime->uptimePercentage,
    ], $uptimes);
    $header = [
      'datetime' => $this->t('Date time'),
      'uptimePercentage' => $this->t('Uptime percentage'),
    ];
    return [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $parsed,
    ];
  }

}
