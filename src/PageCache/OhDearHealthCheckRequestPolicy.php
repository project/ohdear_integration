<?php

namespace Drupal\ohdear_integration\PageCache;

use Drupal\Core\PageCache\RequestPolicyInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Cache policy for OhDear healthcheck endpoint.
 *
 * This policy disallows the page cache module from caching requests that use
 * the oh-dear-health-check-secret header.
 */
class OhDearHealthCheckRequestPolicy implements RequestPolicyInterface {

  /**
   * {@inheritdoc}
   */
  public function check(Request $request) {
    $secret = $request->headers->get('oh-dear-health-check-secret');
    if (isset($secret)) {
      return self::DENY;
    }
    return NULL;
  }

}
