<?php

namespace Drupal\ohdear_integration\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure OhDear integration settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'ohdear_integration_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['ohdear_integration.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $message = '';
    if (getenv('OHDEAR_API_KEY')) {
      $message .= 'Environment variable OHDEAR_API_KEY is set and will override value of API key for the OhDear App. ';
    }
    if (getenv('OHDEAR_HEALTHCHECK_SECRET')) {
      $message .= 'Environment variable OHDEAR_HEALTHCHECK_SECRET is set and will override value of healthcheck secret for the OhDear App. ';
    }
    if (getenv('OHDEAR_SITE_ID')) {
      $message .= 'Environment variable OHDEAR_SITE_ID is set and will override value of the OhDear Site id. ';
    }
    if (getenv('OHDEAR_CRON_URI')) {
      $message .= 'Environment variable OHDEAR_CRON_URI is set and will override the configuration value of the OhDear Cron Uri. ';
    }
    if ($message) {
      $this->messenger()->addWarning($message);
    }
    $ohdear_settings = $this->config('ohdear_integration.settings');
    $form['ohdear_healthcheck_secret'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Secret provided by OhDear App'),
      '#description' => $this->t('Enables access of OhDear App to the <a href="/json/oh-dear-health-check-results" target="_blank">healthcheck endpoint</a>. Healthcheck secret is obtained from the OhDear service. OhDear data is available on <a href="/admin/reports/ohdear/info" target="_blank">info</a> endpoint.'),
      '#default_value' => $ohdear_settings->get('ohdear_healthcheck_secret'),
      '#attributes' => [
        'placeholder' => '4zcke2vxfETABM8h',
      ],
    ];
    $form['ohdear_cron_uri'] = [
      '#type' => 'textfield',
      '#title' => $this->t('OhDear Cron Uri'),
      '#description' => $this->t('Uri to ping when cron is run. Monitoring scheduled tasks is running if this value is configured. Uri should be obtained from the OhDear service.'),
      '#default_value' => $ohdear_settings->get('ohdear_cron_uri') ?? '',
      '#attributes' => [
        'placeholder' => 'https://ping.ohdear.app/db4a0676-f25e-3328-b325-0d8a5c58fac1',
      ],
    ];
    $form['ohdear_api_key'] = [
      '#type' => 'password',
      '#title' => $this->t('API key for the OhDear App'),
      '#description' => $this->t('Enables access of OhDear App API. API key is obtained from the OhDear service. For more info read the <a href="https://ohdear.app/docs/integrations/the-oh-dear-api#get-your-api-token" target="_blank">OhDear API docs</a>. If API key is not provided, some features of this module will not work. API key can also be set with php environment variable <strong>OHDEAR_API_KEY</strong>. If this environment variable is set, it takes precedence to configuration setting.'),
      '#default_value' => $ohdear_settings->get('ohdear_api_key'),
      '#attributes' => [
        'placeholder' => '',
      ],
    ];
    $form['ohdear_site_id'] = [
      '#type' => 'number',
      '#title' => $this->t('OhDear Site id'),
      '#description' => $this->t('Site ID is obtained from the OhDear service. For more info read the <a href="https://ohdear.app/docs/integrations/the-oh-dear-api#sites" target="_blank">OhDear api docs</a>. If site id is wrong, maintenance window drush command will not work!'),
      '#default_value' => $ohdear_settings->get('ohdear_site_id'),
      '#attributes' => [
        'placeholder' => '30556',
      ],
    ];
    // @todo Add validation check for users to check if API key provides
    // authentication and site id links to correct site.
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('ohdear_integration.settings')
      ->set('ohdear_healthcheck_secret', $form_state->getValue('ohdear_healthcheck_secret'))
      ->set('ohdear_cron_uri', $form_state->getValue('ohdear_cron_uri'))
      ->set('ohdear_site_id', $form_state->getValue('ohdear_site_id'));
    // Only re-save api key if new value is provided.
    if ($new_api_key = $form_state->getValue('ohdear_api_key')) {
      $config->set('ohdear_api_key', $new_api_key);
    }
    $config->save();
    parent::submitForm($form, $form_state);
  }

}
