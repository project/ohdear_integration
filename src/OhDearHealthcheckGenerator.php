<?php

namespace Drupal\ohdear_integration;

use Drupal\Component\Utility\Html;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\monitoring\Entity\SensorResultDataInterface;
use Drupal\monitoring\Result\SensorResultInterface;
use Drupal\monitoring\SensorRunner;
use OhDear\HealthCheckResults\CheckResult;
use OhDear\HealthCheckResults\CheckResults;

/**
 * Converts monitoring sensor result objects OhDear health checks.
 */
class OhDearHealthcheckGenerator {

  const STATUS_MAPPING = [
    SensorResultDataInterface::STATUS_OK => CheckResult::STATUS_OK,
    SensorResultDataInterface::STATUS_CRITICAL => CheckResult::STATUS_FAILED,
    SensorResultDataInterface::STATUS_UNKNOWN => CheckResult::STATUS_SKIPPED,
    SensorResultDataInterface::STATUS_WARNING => CheckResult::STATUS_WARNING,
    SensorResultDataInterface::STATUS_INFO => CheckResult::STATUS_OK,
  ];

  /**
   * The sensor runner service.
   *
   * @var \Drupal\monitoring\SensorRunner
   */
  protected $sensorRunner;

  /**
   * The logger factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * Check results.
   *
   * @var \OhDear\HealthCheckResults\CheckResults
   */
  protected $checkResults;

  /**
   * Constructs an OhdearHealthcheckGenerator object.
   *
   * @param \Drupal\monitoring\SensorRunner $sensor_runner
   *   The sensor runner service.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $loggerFactory
   *   The logger factory.
   */
  public function __construct(SensorRunner $sensor_runner, LoggerChannelFactoryInterface $loggerFactory) {
    $this->sensorRunner = $sensor_runner;
    $this->loggerFactory = $loggerFactory;
  }

  /**
   * Converts sensor result to ohdear healthcheck.
   *
   * @param \Drupal\monitoring\Result\SensorResultInterface $sensorResult
   *   Monitoring sensor result.
   *
   * @return \OhDear\HealthCheckResults\CheckResult
   *   Object that ohdear service expects.
   */
  public function convertSensorResultToOhdearHealthcheck(SensorResultInterface $sensorResult) {
    $sensor_id = $sensorResult->getSensorId();
    $sensor_config = $sensorResult->getSensorConfig();
    $label = $sensor_config->getLabel();
    $shortSummary = $sensor_config->getDescription() ?? '';
    $notificationMessage = Html::decodeEntities($sensorResult->getMessage());
    if ($value = $sensorResult->getValue()) {
      $meta = [
        'value' => $value,
        'value_label' => $sensor_config->getValueLabel(),
      ];
    }
    else {
      $meta = [];
    }
    $status = self::STATUS_MAPPING[$sensorResult->getStatus()] ?? CheckResult::STATUS_SKIPPED;
    return $this->getCheckResult($sensor_id, $label, $notificationMessage, $shortSummary, $status, $meta);
  }

  /**
   * Get CheckResults object.
   *
   * @return \OhDear\HealthCheckResults\CheckResults
   *   The check results object.
   */
  public function getCheckResults() {
    if (!isset($this->checkResults)) {
      $this->checkResults = new CheckResults();
    }
    return $this->checkResults;
  }

  /**
   * Create one result object.
   *
   * @param string $name
   *   Healthcheck name.
   * @param string $label
   *   Healthcheck label.
   * @param string $notificationMessage
   *   Notification message.
   * @param string $shortSummary
   *   Short summary.
   * @param string $status
   *   Status.
   * @param array $meta
   *   Additional information.
   *
   * @return \OhDear\HealthCheckResults\CheckResult
   *   The check result object.
   */
  public function getCheckResult(string $name, string $label = '', string $notificationMessage = '', string $shortSummary = '', string $status = '', array $meta = []) {
    return new CheckResult($name, $label, $notificationMessage, $shortSummary, $status, $meta);
  }

  /**
   * Runs all enabled sensors and converts them to OhDear healthchecks.
   *
   * @return string
   *   Json data of all monitoring sensor results converted to healthchecks.
   */
  public function getData() {
    $checkResults = $this->getCheckResults();
    $results = $this->sensorRunner->runSensors();
    // The check results field must not have more than 50 items.
    if (count($results) > 50) {
      $logger = $this->loggerFactory->get('ohdear_integration');
      $logger->warning('The check results field must not have more than 50 items. Please disable some sensors in the configuration of the Monitoring module.');
      $results = array_slice($results, 0, 50);
    }
    foreach ($results as $result) {
      $checkResults->addCheckResult($this->convertSensorResultToOhdearHealthcheck($result));
    }
    return $checkResults->toJson();
  }

}
