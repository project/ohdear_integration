<?php

namespace Drupal\ohdear_integration;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Logger\LoggerChannelTrait;
use OhDear\PhpSdk\OhDear;

/**
 * Service for easier handling of OhDear php sdk.
 *
 * Serves as a bridge between OhDear php sdk and drupal.
 */
class OhDearSdkService {
  use LoggerChannelTrait;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected ConfigFactoryInterface $configFactory;

  /**
   * The OhDear Sdk object.
   *
   * @var \OhDear\PhpSdk\OhDear
   */
  protected $ohdear;

  /**
   * The ohdear site id.
   *
   * @var int
   */
  protected $siteId;

  /**
   * The ohdear site.
   *
   * @var \OhDear\PhpSdk\Resources\Site
   */
  protected $ohDearSite;

  /**
   * The OhDear Healthcheck secret.
   *
   * @var string|false
   */
  protected $ohDearHealthcheckSecret;

  /**
   * The OhDear Cron uri.
   *
   * @var string|false
   */
  protected $ohDearCronUri;

  /**
   * Value to be used in max-age property of Cache-Control header.
   *
   * @var string|int|null
   */
  protected $ohDearHealthcheckCacheMaxAge;

  /**
   * Constructs an OhDearSdkService object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $loggerFactory
   *   The logger channel factory service.
   */
  public function __construct(ConfigFactoryInterface $configFactory, LoggerChannelFactoryInterface $loggerFactory) {
    $this->configFactory = $configFactory;
    $this->setLoggerFactory($loggerFactory);
  }

  /**
   * Creates OhDear sdk object.
   *
   * @return \OhDear\PhpSdk\OhDear
   *   The OhDear php sdk object.
   */
  public function getOhDear() {
    if (isset($this->ohdear)) {
      return $this->ohdear;
    }
    $apiKey = getenv('OHDEAR_API_KEY') ?: $this->configFactory->get('ohdear_integration.settings')->get('ohdear_api_key');
    if ($apiKey) {
      $this->ohdear = new OhDear($apiKey);
      return $this->ohdear;
    }
    $this->getLogger('ohdear_integration')->error('OhDear API key is missing!');
    throw new \Exception('OhDear API key is missing.');
  }

  /**
   * Reads site id from the config and overrides it with environment variable.
   *
   * @return int
   *   The site id.
   *
   * @throws \Exception
   */
  public function getSiteId() {
    if (isset($this->siteId)) {
      return $this->siteId;
    }
    $siteId = getenv('OHDEAR_SITE_ID') ?: $this->configFactory->get('ohdear_integration.settings')->get('ohdear_site_id');
    $siteId = (int) $siteId;
    if (!$siteId) {
      $this->getLogger('ohdear_integration')->error('OhDear site id is missing! Fix it at <a href="/admin/config/system/ohdear-settings">OhDear Integration configuration form</a>.');
      throw new \Exception('OhDear site id is missing.');
    }
    $this->siteId = (int) $siteId;
    return $this->siteId;
  }

  /**
   * Returns the OhDear php sdk site object.
   *
   * @return \OhDear\PhpSdk\Resources\Site
   *   The OhDear site for current site id.
   *
   * @throws \Exception
   */
  public function getSite() {
    if (isset($this->ohDearSite)) {
      return $this->ohDearSite;
    }
    try {
      $site = $this->getOhDear()->site($this->getSiteId());
      $this->ohDearSite = $site;
      return $this->ohDearSite;
    }
    catch (\Throwable $e) {
      $this->getLogger('ohdear_integration')->error('OhDear site not found! Fix it at <a href="/admin/config/system/ohdear-settings">OhDear Integration configuration form</a>.');
      throw new \Exception('OhDear site not found.');
    }
  }

  /**
   * Returns healthcheck secret configuration option with possible override.
   *
   * @return false|string
   *   The configuration value of false if not set.
   */
  public function getHealthcheckSecret() {
    if (isset($this->ohDearHealthcheckSecret)) {
      return $this->ohDearHealthcheckSecret;
    }
    $this->ohDearHealthcheckSecret = getenv('OHDEAR_HEALTHCHECK_SECRET') ?: $this->configFactory->get('ohdear_integration.settings')->get('ohdear_healthcheck_secret');
    return $this->ohDearHealthcheckSecret;
  }

  /**
   * Returns the max age configuration for healthcheck json endpoint.
   *
   * If not provided, caching of healthcheck endpoint is considered disabled.
   *
   * @return mixed
   *   Null or integer value.
   */
  public function getHealthcheckCacheMaxAge() {
    if (isset($this->ohDearHealthcheckCacheMaxAge)) {
      return $this->ohDearHealthcheckCacheMaxAge;
    }
    $this->ohDearHealthcheckCacheMaxAge = $this->configFactory->get('ohdear_integration.settings')->get('healthcheck_cache_max_age') ?? NULL;
    return $this->ohDearHealthcheckCacheMaxAge;
  }

  /**
   * Returns healthcheck secret configuration option with possible overridden.
   *
   * @return false|string
   *   The configuration value of false if not set.
   */
  public function getCronUri() {
    if (isset($this->ohDearCronUri)) {
      return $this->ohDearCronUri;
    }
    $this->ohDearCronUri = getenv('OHDEAR_CRON_URI') ?: $this->configFactory->get('ohdear_integration.settings')->get('ohdear_cron_uri');
    return $this->ohDearCronUri;
  }

}
