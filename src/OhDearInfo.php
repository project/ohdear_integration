<?php

namespace Drupal\ohdear_integration;

use OhDear\PhpSdk\Resources\Site;

/**
 * Service to retrieve info from OhDear api.
 */
class OhDearInfo {

  /**
   * The OhDear php service development kit object.
   *
   * @var \Drupal\ohdear_integration\OhDearSdkService
   */
  protected OhDearSdkService $ohDearSdk;

  /**
   * Construct method.
   *
   * @param \Drupal\ohdear_integration\OhDearSdkService $ohDearSdkService
   *   OhDear php sdk.
   */
  public function __construct(OhDearSdkService $ohDearSdkService) {
    $this->ohDearSdk = $ohDearSdkService;
  }

  /**
   * Get a list of sites available with current OhDear API key.
   *
   * @return \OhDear\PhpSdk\Resources\Site[]
   *   Array of sites with id and url properties.
   */
  public function listSites() {
    return $this->ohDearSdk->getOhDear()->sites();
  }

  /**
   * Get site label.
   *
   * @param int|null $siteId
   *   Site id or falsy for current site.
   *
   * @return string
   *   Return site label or url.
   */
  public function getSiteLabel(?int $siteId) {
    return $this->getSite($siteId)->label ?? $this->getSite($siteId)->url;
  }

  /**
   * Get an option list of sites.
   *
   * @return array
   *   Array of site ids as keys and labels as values.
   */
  public function prettySiteList() {
    return array_map(fn ($site) => [$site->id => $site->label ?? $site->url], $this->listSites());
  }

  /**
   * Get uptimes - default value is last 30 days.
   *
   * @param int $siteId
   *   The site id.
   * @param string $from
   *   Start date with 'YmdHis' format.
   * @param string $to
   *   End date with 'YmdHis' format.
   * @param string $split
   *   Time interval (hour, day, month, year).
   *
   * @return \OhDear\PhpSdk\Resources\Uptime[]
   *   Array of uptimes.
   */
  public function getUptime(?int $siteId = NULL, ?string $from = NULL, ?string $to = NULL, string $split = 'day') {
    if (!$from) {
      $from = date('YmdHis', strtotime('-30 days'));
    }
    if (!$to || $to < $from) {
      $to = date('YmdHis');
    }
    // @todo Implement validation for input parameters.
    return $this->getSite()->uptime($from, $to, $split);
  }

  /**
   * Get broken links for site.
   *
   * @return \OhDear\PhpSdk\Resources\BrokenLink[]
   *   Array of broken links.
   */
  public function getBrokenLinks(?int $siteId = NULL) {
    return $this->getSite($siteId)->brokenLinks();
  }

  /**
   * Get OhDear checks for site.
   *
   * @param int|null $siteId
   *   The site id to check.
   *
   * @return \OhDear\PhpSdk\Resources\Check[]
   *   Array of checks.
   */
  public function getChecks(?int $siteId = NULL) {
    return $this->getSite($siteId)?->attributes['checks'] ?? [];
  }

  /**
   * Get site helper method.
   *
   * Returns the configured site or the one provided by siteId argument which
   * can be different but should be accessible with the configured OhDear API
   * key.
   *
   * @param int|null $siteId
   *   Site id or null if you want to get the default one.
   *
   * @return \OhDear\PhpSdk\Resources\Site
   *   The OhDear php sdk site object instance.
   */
  protected function getSite(?int $siteId = NULL): Site {
    return $siteId ? $this->ohDearSdk->getOhDear()->site($siteId) : $this->ohDearSdk->getSite();
  }

}
