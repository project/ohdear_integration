# OhDear Integration

Integrates your drupal website with the [OhDear](https://ohdear.app) monitoring app.

### Healthchecks

Provides `json/oh-dear-health-check-results` endpoint for OhDear healthchecks.
These are converted sensor results from [Monitoring module](https://drupal.org/project/monitoring).
Endpoint requires configuration of OhDear Healthcheck Secret that enables
access of OhDear service to the endpoint.

### Scheduled Tasks

With configuring the OhDear cron uri, website will start to send data on cron
runs to OhDear service. Ping uri is first obtained on OhDear under Scheduled
tasks tab.

### Drush commands

This module provides two drush commands to work with OhDear API.
`drush ohdear:maintenance` allows you to inspect/start/stop
[maintenance periods](https://ohdear.app/docs/features/how-to-configure-maintenance-windows)
on OhDear App.

`drush ohdear:info` prints basic info about configured site.

`drush ohdear:uptime` prints daily uptime percentages for last 7 days.

`drush ohdear:broken-links` provides list of broken links found by OhDear.

### Most valuable data

Most valuable data from OhDear is also available directly on your drupal at
these paths:
- `/admin/reports/ohdear/info` provides the OhDear checks results for current
site.
- `/admin/reports/ohdear/uptime` provides uptime percentages. By default, it
shows daily percentages for last 7 days, but this can be changed with providing
`from` and `to` query parameters in `'YmdHis'` date format. Time interval can
be changed by providing `split` query parameter. It can have values `hour`,
`day` or `month`.
- `/admin/reports/ohdear/broken-links` provides a list of broken links that
OhDear service found on the website.

## Configuration

### API key and site id

API key enables access of OhDear App API. API key is obtained from the OhDear
service. For more info read the
[OhDear API docs](https://ohdear.app/docs/integrations/the-oh-dear-api#get-your-api-token).
If API key is not provided, some features (mainly drush commands) of this
module will not work.

API key can also be provided by php environment variable `OHDEAR_API_KEY`. If
this variable is set it will override the value set in configuration.

Site id could also be set in configuration and can be overridden (or set) with
the `OHDEAR_SITE_ID` environment variable.

### Healthchecks

`/admin/config/system/ohdear-settings` configure `oh-dear-health-check-secret`
and enable or disable monitoring sensors plugins. Secret is provided by OhDear
service at `ohdear.app/sites/<site-id>/settings/application-health`.

Enabled monitoring sensors are converted into OhDear Healthchecks. Monitoring
sensors are configured at /admin/config/system/monitoring/settings. For nicer
health checks overview we've developed a sensor that combines requirements
hook (called RequirementsRecap). We recommend using it.

Healthchecks secret can also be set as an environment variable
`OHDEAR_HEALTHCHECK_SECRET`. If this variable is set it will override the value
set in configuration.

### Cron

Obtain the ping uri under Scheduled tasks tab on OhDear app
(`https://ohdear.app/sites/<site-id>/check/scheduled-tasks`) and set it in
settings form. OhDear integration module will start to notify OhDear service
about cron runs. Currently only default drupal cron is supported.

The cron ping uri can be overridden with `OHDEAR_CRON_URI` environment
variable.

### Caching

The healthcheck results endpoint allows caching but it is disabled by default.
To enable the caching manually add `healthcheck_cache_max_age` to the
`ohdear_integration.settings.yml` config file with the value in seconds how
long the resource should be cached.

If `healthcheck_cache_max_age` configuration option is not provided, the
resource is not cached.

For this functionality to work properly requests to `/json/oh-dear-health-check-results`
endpoint should come with the secret provided as URL query parameter and not
as secret header - if you have Drupal's internal page cache installed. Internal
page cache does not respect Cache contexts. One tricky way to resolve this issue
is to add a redirect for requests to endpoint with the `oh-dear-health-check-secret`
header to the same endpoint but with the URL query parameter
`?oh-dear-health-check-secret=<value-of-header>`.
