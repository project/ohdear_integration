<?php

namespace Drupal\Tests\ohdear_integration\Functional;

use Drupal\Component\Serialization\Json;
use Drupal\Tests\BrowserTestBase;

/**
 * Test OhDear Integration endpoint.
 *
 * @group ohdear_integration
 */
class OhdearIntegrationEndpointTest extends BrowserTestBase {

  /**
   * A user with administrative permissions.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $adminUser;

  /**
   * Modules to install.
   *
   * @var array
   */
  protected static $modules = [
    'monitoring',
    'monitoring_test',
    'ohdear_integration',
    'node',
  ];

  /**
   * Secret to access ohdear integration endpoint.
   *
   * @var string
   */
  protected $ohdearSecret = '4zcke2vxfETABM8h';

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->config('ohdear_integration.settings')->set('ohdear_healthcheck_secret', $this->ohdearSecret)->save();
    $this->config('ohdear_integration.settings')->set('ohdear_site_id', 999999)->save();

    // Create admin user.
    $this->adminUser = $this->drupalCreateUser([
      'access administration pages',
      'monitoring reports',
      'access ohdear info',
    ]);
  }

  /**
   * Testing healthcheck results json endpoint.
   */
  public function testApiResponseData() {
    // Anonymous request.
    $this->drupalGet('json/oh-dear-health-check-results');
    $this->assertSession()->statusCodeEquals(403);
    // Anonymous request with header secret.
    $options['query'] = ['_format' => 'json'];
    drupal_flush_all_caches();
    $output = $this->drupalGet('json/oh-dear-health-check-results', $options, ['oh-dear-health-check-secret' => $this->ohdearSecret]);
    $this->assertSession()->statusCodeEquals(200);
    $data = Json::decode($output ?? []);
    $this->assertNotEmpty($data['checkResults'] ?? [], 'Check results are missing.');
    $checkResults = $this->parseCheckResults($data);
    $this->assertContains('test_sensor_exceeds', array_keys($checkResults), 'Test sensor exceeds not found.');
    // Anonymous request with wrong header secret.
    drupal_flush_all_caches();
    $this->drupalGet('json/oh-dear-health-check-results', $options, ['oh-dear-health-check-secret' => '111li3uygIRABaaa']);
    $this->assertSession()->statusCodeEquals(403);
    // Administrator account should also have access.
    drupal_flush_all_caches();
    $this->drupalLogin($this->adminUser);
    $output = $this->drupalGet('json/oh-dear-health-check-results', $options);
    $this->assertSession()->statusCodeEquals(200);
    $data = Json::decode($output ?? []);
    $this->assertNotEmpty($data['checkResults'] ?? [], 'Check results are missing.');
    $checkResults = $this->parseCheckResults($data);
    $this->assertContains('test_sensor', array_keys($checkResults), 'Test sensor not found');
  }

  /**
   * Test /admin/reports/ohdear/info.
   */
  public function testInfoRoute() {
    $this->drupalGet('/admin/reports/ohdear/info');
    $this->assertNotEquals(200, $this->getSession()->getStatusCode(), 'Status code for ohdear info for anonymous request is not 403.');
    $this->drupalLogin($this->adminUser);
    // We can not test without a valid site id.
  }

  /**
   * Parse endpoint data response.
   *
   * Gives us option to add more detailed checks of sensors.
   *
   * @param array $data
   *   Json response of healthcheck endpoint.
   *
   * @return array
   *   Array of healthcheck results keyed with sensor name.
   */
  protected function parseCheckResults(array $data) {
    $checkResults = [];
    foreach ($data['checkResults'] as $checkResult) {
      $checkResults[$checkResult['name']] = $checkResult;
    }
    return $checkResults;
  }

}
